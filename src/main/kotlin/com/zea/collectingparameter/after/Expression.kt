package com.zea.collectingparameter.after

interface Expression {
 fun toString(objectPrinter: ObjectPrinter)
}
