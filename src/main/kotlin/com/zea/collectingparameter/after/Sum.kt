package com.zea.collectingparameter.after

class Sum(val augend: Expression, val addend: Expression) : Expression{

    override fun toString(): String {
        val writer = ObjectPrinter()
        toString(writer)
        return writer.contents()
    }

    override fun toString(objectPrinter: ObjectPrinter) {
        objectPrinter.add("+")
        objectPrinter.indent()
        augend.toString(objectPrinter)
        objectPrinter.indent()
        addend.toString(objectPrinter)
    }
}
