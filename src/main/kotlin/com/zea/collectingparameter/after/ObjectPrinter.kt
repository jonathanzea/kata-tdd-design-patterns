package com.zea.collectingparameter.after

class ObjectPrinter {
    private var content = ""

    fun add(char: String) {
        content += char
    }

    fun indent() {
        content += "\n\t"
    }

    fun contents(): String {
        return content
    }
}
