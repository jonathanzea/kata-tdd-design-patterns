package com.zea.collectingparameter.after

class Money(private val value: String): Expression {

    override fun toString(objectPrinter: ObjectPrinter) {
        objectPrinter.add(value)
    }
}