package com.zea.collectingparameter.before

class Sum(val augend: String, val addend: String) {

    override fun toString(): String {
        return "$augend + $addend"
    }
}
