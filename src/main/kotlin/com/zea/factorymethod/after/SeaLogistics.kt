package com.zea.factorymethod.after

class SeaLogistics : Logistics {

    override fun createTransport(): Transport {
        return Boat()
    }
}
