package com.zea.factorymethod.after

interface Logistics {

    fun createTransport(): Transport
}