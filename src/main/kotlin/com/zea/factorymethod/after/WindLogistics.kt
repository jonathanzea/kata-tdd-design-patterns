package com.zea.factorymethod.after

class WindLogistics : Logistics {
    override fun createTransport(): Transport {
        return Airplane()
    }
}