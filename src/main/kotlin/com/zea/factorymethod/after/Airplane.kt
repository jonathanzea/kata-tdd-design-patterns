package com.zea.factorymethod.after

class Airplane : Transport {

    override fun doAction(): String {
        return "Flying through the clouds..."
    }
}
