package com.zea.factorymethod.after

class Boat : Transport {

    override fun doAction(): String {
        return "Navigating in the waters..."
    }
}
