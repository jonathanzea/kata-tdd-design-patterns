package com.zea.factorymethod.after

interface Transport {
    fun doAction(): String
}
