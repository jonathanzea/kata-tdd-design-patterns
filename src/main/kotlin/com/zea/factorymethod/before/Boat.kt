package com.zea.factorymethod.before

class Boat {

    fun doAction(): String {
        return "Navigating in the waters..."
    }
}
