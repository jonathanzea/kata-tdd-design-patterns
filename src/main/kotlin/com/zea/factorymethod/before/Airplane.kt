package com.zea.factorymethod.before

class Airplane {

    fun doAction(): String {
        return "Flying through the clouds..."
    }
}
