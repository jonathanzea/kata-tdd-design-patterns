package com.zea.factorymethod.before

class SeaLogistics(val boat: Boat) {

    fun doTransportAction(): String {
        return boat.doAction()
    }
}
