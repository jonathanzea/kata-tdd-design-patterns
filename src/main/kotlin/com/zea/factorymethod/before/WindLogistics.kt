package com.zea.factorymethod.before

class WindLogistics(val airplane: Airplane) {

    fun doTransportAction(): String {
        return airplane.doAction()
    }
}
