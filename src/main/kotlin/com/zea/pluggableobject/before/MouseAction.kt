package com.zea.pluggableobject.before

class MouseAction(val selection: Selection) {
    var log: String = ""

    fun initAndLog() {
        mouseDown()
        mouseMove()
        mouseUp()
    }

    private fun mouseDown() {
        log += if(selection.figure.isBlank()) {
            "Selector square initialized, "
        } else {
            "${selection.figure} selected, "
        }
    }

    private fun mouseMove() {
        log += if(selection.figure.isBlank()) {
            "selection resized, "
        } else {
            "${selection.figure} moved, "
        }
    }

    private fun mouseUp() {
        log += if (selection.figure.isBlank()) {
            "performed selectAll()."
        } else {
            "${selection.figure} unselected."
        }
    }
}
