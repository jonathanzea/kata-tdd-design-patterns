package com.zea.pluggableobject.after

class MouseAction(private var selectionMode: SelectionMode) {
    var log: String = ""

    fun initAndLog() {
        mouseDown()
        mouseMove()
        mouseUp()
    }

    private fun mouseDown() {
        log += selectionMode.click()
    }

    private fun mouseMove() {
        log += selectionMode.move()
    }

    private fun mouseUp() {
        log += selectionMode.unclick()
    }
}
