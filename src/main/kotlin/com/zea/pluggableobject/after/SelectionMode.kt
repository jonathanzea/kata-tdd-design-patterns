package com.zea.pluggableobject.after

interface SelectionMode {
    fun selection(): String
    fun click(): String
    fun move(): String
    fun unclick(): String
}