package com.zea.pluggableobject.after

class MultipleSelection() : SelectionMode {
    override fun selection() = ""

    override fun click() = "Selector square initialized, "

    override fun move() = "selection resized, "

    override fun unclick() = "performed selectAll()."
}