package com.zea.pluggableobject.after

class SingleSelection(private val selection: Selection) :
    SelectionMode {
    override fun selection() = selection.figure

    override fun click() = "${selection()} selected, "

    override fun move() = "${selection()} moved, "

    override fun unclick() = "${selection()} unselected."
}