package com.zea.imposter.after

interface FileGenerator {
    fun callFileGeneratorServiceThatMightFail(): String
}
