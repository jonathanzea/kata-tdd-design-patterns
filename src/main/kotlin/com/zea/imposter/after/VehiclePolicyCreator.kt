package com.zea.imposter.after

class VehiclePolicyCreator(val fileGenerator: FileGenerator) {
    private var status: String = ""

    fun createDocumentFile(){
        status = fileGenerator.callFileGeneratorServiceThatMightFail()
    }

    fun checkStatus() = status
}
