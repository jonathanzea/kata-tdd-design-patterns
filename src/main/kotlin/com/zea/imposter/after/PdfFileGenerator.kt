package com.zea.imposter.after

class PdfFileGenerator : FileGenerator {

    override fun callFileGeneratorServiceThatMightFail(): String {
        return "Policy.pdf"
    }
}
