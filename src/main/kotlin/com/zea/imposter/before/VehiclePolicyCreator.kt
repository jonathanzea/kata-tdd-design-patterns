package com.zea.imposter.before

class VehiclePolicyCreator(private val fileGenerator: FileGenerator) {
    private var status: String = ""

    fun createDocumentFile(){
        status = fileGenerator.callFileGeneratorServiceThatMightFail()
    }

    fun checkStatus() = status
}
