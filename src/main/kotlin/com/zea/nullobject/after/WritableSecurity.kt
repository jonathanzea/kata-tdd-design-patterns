package com.zea.nullobject.after

class WritableSecurity : SomeSecurity {
    override fun call(): String {
        return "File modified"
    }
}
