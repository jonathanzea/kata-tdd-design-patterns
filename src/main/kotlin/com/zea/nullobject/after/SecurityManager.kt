package com.zea.nullobject.after

class SecurityManager(private val systemSecurityIndicator: String?) {
    fun getSecurity(): SomeSecurity {
        return if (systemSecurityIndicator != null ) WritableSecurity() else LaxSecurity()
    }
}