package com.zea.nullobject.after

class SpecialFile(private val security: SomeSecurity?) {
    var passed: Boolean = false
    var status: String = ""

    fun open() {
        status = this.security!!.call()
        passed = true
    }
}