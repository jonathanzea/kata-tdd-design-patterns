package com.zea.nullobject.before

class SpecialFile(private val security: SomeSecurity?) {
    var passed: Boolean = false
    var status: String = ""

    fun open() {
        if(security == null){
            status = ""
        }else{
            status = security.call()
        }
        passed = true
    }
}