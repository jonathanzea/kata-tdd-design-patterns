package com.zea.nullobject.before

class WritableSecurity : SomeSecurity {
    override fun call(): String {
        return "File modified"
    }
}
