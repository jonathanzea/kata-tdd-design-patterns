package com.zea.singleton.after

import java.util.concurrent.ThreadLocalRandom

object EmailServiceConfiguration {
    val memoryLocation = ThreadLocalRandom.current().nextInt()
}
