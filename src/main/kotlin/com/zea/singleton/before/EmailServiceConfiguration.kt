package com.zea.singleton.before

import java.util.concurrent.ThreadLocalRandom

class EmailServiceConfiguration(val memoryLocation: Int = ThreadLocalRandom.current().nextInt())
