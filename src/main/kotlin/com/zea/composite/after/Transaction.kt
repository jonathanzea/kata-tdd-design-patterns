package com.zea.composite.after

import com.zea.composite.after.Holding

class Transaction(val value: Int) : Holding {
    override fun balance(): Int {
        return value
    }

}
