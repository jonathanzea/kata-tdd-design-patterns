package com.zea.composite.after

class Account(val transactions: List<Holding>) :
    Holding {

    override fun balance(): Int {
        return transactions.map { transaction -> transaction.balance() }.sum()
    }
}
