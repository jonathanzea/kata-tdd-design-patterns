package com.zea.composite.after

interface Holding {
    fun balance(): Int
}
