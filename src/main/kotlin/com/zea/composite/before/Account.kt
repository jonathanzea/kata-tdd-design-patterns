package com.zea.composite.before

class Account(val transactions: List<Transaction>) {

    fun balance(): Int {
        return transactions.map { transaction -> transaction.value }.sum()
    }
}
