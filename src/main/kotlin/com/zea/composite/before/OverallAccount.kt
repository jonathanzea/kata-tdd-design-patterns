package com.zea.composite.before

class OverallAccount(val accounts: List<Account>) {

    fun balance(): Int {
        return accounts.map { account ->
            (account.transactions.map { transaction ->
                transaction.value
            }).sum()
        }.sum()
    }
}