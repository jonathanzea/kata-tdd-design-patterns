package com.zea.templatemethod.before

class FishTunaService {
    var neatness: String = "tuna full of blood and guts"
    var packaging: String = "tuna still in the nets"
    var exported: Boolean = false

    fun cleanFish() {
        neatness = "tuna cleaned"
    }

    fun packageFish() {
        packaging = "tuna ready in the packages"
    }

    fun exportTuna() {
        exported = true
    }
}
