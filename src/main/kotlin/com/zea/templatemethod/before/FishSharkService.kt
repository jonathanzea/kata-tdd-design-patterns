package com.zea.templatemethod.before

class FishSharkService {
    var neatness: String = "Shark service doesn't clean them"
    var packaging: String = "Shark still in the nets"
    var exported: Boolean = false

    fun packageFish() {
        packaging = "Shark ready in the packages"
    }

    fun exportShark() {
        exported = true
    }
}
