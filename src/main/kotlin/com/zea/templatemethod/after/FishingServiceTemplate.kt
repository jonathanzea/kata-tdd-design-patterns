package com.zea.templatemethod.after

abstract class FishingServiceTemplate() {
    fun execute() {
        cleanFish()
        packageFish()
        exportFish()
    }

    open fun cleanFish() {

    }

    abstract fun packageFish()
    abstract fun exportFish()
}
