package com.zea.templatemethod.after

class FishShrimpService : FishingServiceTemplate() {
    var neatness: String = "Shrimp service does not clean them"
    var packaging: String = "Shrimps still in the nets"
    var exported: Boolean = false

    override fun packageFish() {
        packaging = "Shrimps frozen and packaged"
    }

    override fun exportFish() {
        exported = true
    }
}