package com.zea.templatemethod.after

class FishCrabService : FishingServiceTemplate() {
    var neatness: String = "crab full of blood and guts"
    var packaging: String = "crab still in the nets"
    var exported: Boolean = false

    override fun cleanFish() {
        neatness = "crab cleaned"
    }

    override fun packageFish() {
        packaging = "crabs ready in the packages"
    }

    override fun exportFish() {
        exported = true
    }

}

