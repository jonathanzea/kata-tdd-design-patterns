package com.zea.pluggableselector.before

interface Report {
    fun print()
}
