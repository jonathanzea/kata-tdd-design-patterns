package com.zea.pluggableselector.before

class XMLReporter : Report {
    var executed = ""

    override fun print() {
        executed = "<xml>I'm a title</xml>"
    }
}
