package com.zea.pluggableselector.after

class ReportSelector(val printerType: String) {
    var executed = ""

    fun print() {
        val method = javaClass.getMethod(printerType)
        method.invoke(this)
    }

    fun printXML(){
        executed = "<xml>I'm a title</xml>"
    }

    fun printHTML() {
        executed = "<div id= \"bigSquare\"></div>"
    }
}
