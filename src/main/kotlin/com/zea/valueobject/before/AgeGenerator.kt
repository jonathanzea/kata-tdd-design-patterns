package com.zea.valueobject.before

import com.zea.valueobject.before.Age
import java.lang.IllegalArgumentException

class AgeGenerator {
    private fun randomNumber(): Int {
        return -3
    }

    fun getAgeQuantityFromGenerator(): Int {
        val randomNumber = randomNumber()
        if (randomNumber < 0) {
            throw IllegalArgumentException("Age can not be created with negative numbers")
        }
        val age = Age(randomNumber)
        return age.ageQuantity
    }
}

