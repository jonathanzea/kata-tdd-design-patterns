package com.zea.valueobject.after

class AgeGenerator {
    private fun generateErroneousNumber(): Int {
        return -3
    }

    fun getAgeQuantityFromGenerator(): Int {
        val age = Age(generateErroneousNumber())
        return age.quantity
    }
}
