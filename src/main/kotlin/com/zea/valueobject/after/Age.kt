package com.zea.valueobject.after

class Age(val quantity: Int) {
    init {
        require(quantity > 0) { "Age can not be created with negative numbers" }
    }
}
