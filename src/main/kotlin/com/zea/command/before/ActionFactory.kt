package com.zea.command.before

class ActionFactory {

    fun actionPerformed(event: ActionEvent): String {
        if (event is FileNewMenuItem)
            return "fileNewAction() triggered"
        else if (event is FileOpenMenuItem)
            return "fileOpenAction() triggered"
        else if (event is FileOpenRecentMenuItem)
            return "fileOpenRecentAction() triggered"
        else if (event is FileSaveMenuItem)
            return "fileSaveAction() triggered"
        return ""
    }
}
