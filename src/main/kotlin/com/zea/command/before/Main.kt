package com.zea.command.before

fun main() {
    val factory = ActionFactory()
    println(factory.actionPerformed(FileOpenRecentMenuItem()))
}
