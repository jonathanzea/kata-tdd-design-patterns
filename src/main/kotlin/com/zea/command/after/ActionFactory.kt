package com.zea.command.after

class ActionFactory {

    fun actionPerformed(event: ActionEvent): String {
            return (event as Command).execute()
    }
}
