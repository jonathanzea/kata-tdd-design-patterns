package com.zea.command.after

interface Command {

    fun execute(): String
}
