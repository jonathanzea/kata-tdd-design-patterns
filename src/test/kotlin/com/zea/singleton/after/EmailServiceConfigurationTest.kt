package com.zea.singleton.after

import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

class EmailServiceConfigurationTest {

    @Test
    fun `singleton test`() {
        val configuration = EmailServiceConfiguration
        val configurationSecondInstantiation = EmailServiceConfiguration

        assertTrue(configuration.memoryLocation == configurationSecondInstantiation.memoryLocation)
    }
}
