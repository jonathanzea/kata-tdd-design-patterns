package com.zea.singleton.before

import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

class EmailServiceConfigurationTest {
    private val onlyInstanceOfEmailServiceConfiguration = EmailServiceConfiguration()

    @Test
    fun `when an instance gets copied points to the same place`() {
        val configuration1 = onlyInstanceOfEmailServiceConfiguration
        val configuration2 = onlyInstanceOfEmailServiceConfiguration
        assertTrue(configuration1.memoryLocation == configuration2.memoryLocation)
    }

    @Test
    fun `if we do not use the global instance values change`() {
        val configuration1 = EmailServiceConfiguration()
        val configuration2 = EmailServiceConfiguration()
        assertFalse(configuration1.memoryLocation == configuration2.memoryLocation)
    }
}
