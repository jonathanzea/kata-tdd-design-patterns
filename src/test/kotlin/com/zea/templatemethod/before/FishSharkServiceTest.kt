package com.zea.templatemethod.before

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class FishSharkServiceTest {

    @Test
    fun `shark service perform three actions properly`() {
        val fishSharkService = FishSharkService()

        Assertions.assertEquals("Shark service doesn't clean them", fishSharkService.neatness)

        Assertions.assertEquals("Shark still in the nets", fishSharkService.packaging)
        fishSharkService.packageFish()
        Assertions.assertEquals("Shark ready in the packages", fishSharkService.packaging)

        Assertions.assertFalse(fishSharkService.exported)
        fishSharkService.exportShark()
        Assertions.assertTrue(fishSharkService.exported)
    }
}
