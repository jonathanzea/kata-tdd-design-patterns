package com.zea.templatemethod.before

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

class FishTunaServiceTest {

    @Test
    fun `tuna service perform three actions properly`() {
        val fishTunaService = FishTunaService()

        assertEquals("tuna full of blood and guts", fishTunaService.neatness)
        fishTunaService.cleanFish()
        assertEquals("tuna cleaned", fishTunaService.neatness)

        assertEquals("tuna still in the nets", fishTunaService.packaging)
        fishTunaService.packageFish()
        assertEquals("tuna ready in the packages", fishTunaService.packaging)

        assertFalse(fishTunaService.exported)
        fishTunaService.exportTuna()
        assertTrue(fishTunaService.exported)
    }
}
