package com.zea.templatemethod.after

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class FishingServiceTemplateTest {

    @Test
    fun `a template execute the crab service in order`() {
        val fishCrabService = FishCrabService()
        Assertions.assertEquals("crab full of blood and guts", fishCrabService.neatness)
        Assertions.assertEquals("crab still in the nets", fishCrabService.packaging)
        Assertions.assertEquals(false, fishCrabService.exported)

        fishCrabService.execute()

        Assertions.assertEquals("crab cleaned", fishCrabService.neatness)
        Assertions.assertEquals("crabs ready in the packages", fishCrabService.packaging)
        Assertions.assertEquals(true, fishCrabService.exported)
    }

    @Test
    fun `a template execute the shrimp service in order`() {
        val fishShrimpService = FishShrimpService()
        Assertions.assertEquals("Shrimp service does not clean them", fishShrimpService.neatness)
        Assertions.assertEquals("Shrimps still in the nets", fishShrimpService.packaging)
        Assertions.assertEquals(false, fishShrimpService.exported)

        fishShrimpService.execute()

        Assertions.assertEquals("Shrimp service does not clean them", fishShrimpService.neatness)
        Assertions.assertEquals("Shrimps frozen and packaged", fishShrimpService.packaging)
        Assertions.assertEquals(true, fishShrimpService.exported)
    }
}
