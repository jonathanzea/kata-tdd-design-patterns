package com.zea.pluggableobject.before

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class MouseActionTest {

    @Test
    fun `logs properly for a square selection`() {
        val selection = Selection("Square")
        val action = MouseAction(selection)
        action.initAndLog()
        assertEquals(
            "Square selected, Square moved, Square unselected.",
            action.log
        )
    }

    @Test
    fun `logs properly for an empty selection`() {
        val selection = Selection("")
        val action = MouseAction(selection)
        action.initAndLog()
        assertEquals(
            "Selector square initialized, selection resized, performed selectAll().",
            action.log
        )
    }
}
