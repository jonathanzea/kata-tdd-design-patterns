package com.zea.pluggableobject.after

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class MouseActionTest {

    @Test
    fun `logs properly for a square selection`() {
        val action = MouseAction(SingleSelection(Selection("Square")))
        action.initAndLog()
        assertEquals(
            "Square selected, Square moved, Square unselected.",
            action.log
        )
    }

    @Test
    fun `logs properly for an empty selection`() {
        val action = MouseAction(MultipleSelection())
        action.initAndLog()
        assertEquals(
            "Selector square initialized, selection resized, performed selectAll().",
            action.log
        )
    }
}
