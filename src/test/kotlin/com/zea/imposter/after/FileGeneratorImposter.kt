package com.zea.imposter.after

import java.lang.IllegalArgumentException

class FileGeneratorImposter : FileGenerator {
    var documentCreationCallTimes = 0
    private var shouldThrow = false

    fun shouldThrow() {
        shouldThrow = true
    }

    private fun throwIfShould() {
        if (shouldThrow)
            throw IllegalArgumentException()
    }

    override fun callFileGeneratorServiceThatMightFail(): String {
        throwIfShould()
        documentCreationCallTimes++
        return "Policy.pdf"
    }
}
