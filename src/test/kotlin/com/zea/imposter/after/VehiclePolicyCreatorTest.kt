package com.zea.imposter.after

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import java.lang.IllegalArgumentException

class VehiclePolicyCreatorTest {

    @Test
    fun `a policy creation`() {
        val fileGenerator = PdfFileGenerator()
        val policyManager = VehiclePolicyCreator(fileGenerator)
        policyManager.createDocumentFile()
        assertEquals("Policy.pdf", policyManager.checkStatus())
    }

    @Test
    fun `a policy creation on file generator failure`() {
        val fileGenerator = FileGeneratorImposter()
        val policyManager = VehiclePolicyCreator(fileGenerator)
        fileGenerator.shouldThrow()
        assertThrows<IllegalArgumentException> {
            policyManager.createDocumentFile()
        }
    }

    @Test
    fun `a policy creation calls just once the file generator failure`() {
        val fileGenerator = FileGeneratorImposter()
        val policyManager = VehiclePolicyCreator(fileGenerator)
        policyManager.createDocumentFile()
        assertTrue(fileGenerator.documentCreationCallTimes == 1)
    }
}
