package com.zea.imposter.before

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class VehiclePolicyCreatorTest {

    @Test
    fun `only the happy case where the document is properly generated`() {
        val fileGenerator = FileGenerator()
        val policyManager = VehiclePolicyCreator(fileGenerator)
        policyManager.createDocumentFile()
        assertEquals("Policy.pdf", policyManager.checkStatus())
    }
}
