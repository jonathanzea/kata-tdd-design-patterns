package com.zea.pluggableselector.after

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class ReportSelectorTest {

    @Test
    fun `prints xml by calling the right method`() {
        val report = ReportSelector("printXML")
        report.print()
        assertEquals("<xml>I'm a title</xml>", report.executed)
    }

    @Test
    fun `prints html by calling the right method`() {
        val report = ReportSelector("printHTML")
        report.print()
        assertEquals("<div id= \"bigSquare\"></div>", report.executed)
    }
}
