package com.zea.pluggableselector.before

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class ReportersTest {

    @Test
    fun `prints XML properly`() {
        val xmlReporter = XMLReporter()
        xmlReporter.print()
        assertEquals("<xml>I'm a title</xml>", xmlReporter.executed)
    }

    @Test
    fun `prints HTML properly`() {
        val htmlReporter = HTMLReporter()
        htmlReporter.print()
        assertEquals("<div id= \"bigSquare\"></div>", htmlReporter.executed)
    }
}
