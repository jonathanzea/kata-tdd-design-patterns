package com.zea.factorymethod.after

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class LogisticsTest {

    @Test
    fun `sea logistics perform boat actions`() {
        val seaLogistics = SeaLogistics()
        val transport = seaLogistics.createTransport()
        assertEquals("Navigating in the waters...", transport.doAction())
    }

    @Test
    fun `wind logistics perform airplane actions`() {
        val seaLogistics = WindLogistics()
        val transport = seaLogistics.createTransport()
        assertEquals("Flying through the clouds...", transport.doAction())
    }
}
