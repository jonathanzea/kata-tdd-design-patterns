package com.zea.factorymethod.before

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class LogisticsTest {

    @Test
    fun `a sea logistics perform boat transport actions`() {
        val seaLogistics = SeaLogistics(Boat())
        assertEquals(seaLogistics.doTransportAction(), "Navigating in the waters...")
    }

    @Test
    fun `a wind logistics perform boat transport actions`() {
        val windLogistics = WindLogistics(Airplane())
        assertEquals(windLogistics.doTransportAction(), "Flying through the clouds...")
    }
}
