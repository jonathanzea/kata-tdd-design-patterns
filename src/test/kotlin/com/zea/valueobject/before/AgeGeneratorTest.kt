package com.zea.valueobject.before

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import java.lang.IllegalArgumentException

class AgeGeneratorTest {

    @Test
    fun `generator is failing`() {
       val error = assertThrows<IllegalArgumentException> { AgeGenerator().getAgeQuantityFromGenerator() }
        assertEquals("Age can not be created with negative numbers", error.message)
    }
}