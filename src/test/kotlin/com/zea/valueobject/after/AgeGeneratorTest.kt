package com.zea.valueobject.after

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import java.lang.IllegalArgumentException

class AgeGeneratorTest {

    @Test
    fun `generator is failing`() {
        val error = assertThrows<IllegalArgumentException> {AgeGenerator().getAgeQuantityFromGenerator()}
        Assertions.assertEquals("Age can not be created with negative numbers", error.message)
    }
}
