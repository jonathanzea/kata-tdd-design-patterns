package com.zea.valueobject.after

import com.zea.valueobject.after.Age
import com.zea.valueobject.after.AgeGenerator
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import java.lang.IllegalArgumentException

class AgeTest {

    @Test
    fun `can be created with valid attributes`() {
        Age(4)
    }

    @Test
    fun `cannot be created with negative number`() {
        val error = assertThrows<IllegalArgumentException> { Age(-5) }
        Assertions.assertEquals("Age can not be created with negative numbers", error.message)
    }
}
