package com.zea.collectingparameter.after

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class ObjectPrinterTest {

    @Test
    fun `prints the object content in the expected format`() {
        val sum = Sum(Money("5 USD"), Money("7 CHF"))
        assertEquals("+\n\t5 USD\n\t7 CHF", sum.toString())
    }

    @Test
    fun `testi`() {
        val superSum =
            Sum(
                Sum(Money("1 USD"), Money("1 USD")),
                Money("1 CHF")
            )

        println(superSum.toString())
    }
}
