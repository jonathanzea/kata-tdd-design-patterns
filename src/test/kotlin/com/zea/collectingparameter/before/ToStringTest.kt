package com.zea.collectingparameter.before

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class ToStringTest {

    @Test
    fun `prints the object as expected`() {
        val sum = Sum("5 USD", "7 CHF")
        assertEquals("5 USD + 7 CHF", sum.toString())
    }
}
