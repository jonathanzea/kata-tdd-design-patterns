package com.zea.composite.after

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class HoldingTest {

    @Test
    fun `a holding transaction expose a balance`() {
        val transaction = Transaction(20)
        assertEquals(20, transaction.balance())
    }

    @Test
    fun `a holding account exposes a balance of its holding transactions`() {
        val account = Account(listOf(
            Transaction(50),
            Transaction(20)
        ))
        assertEquals(70, account.balance())
    }

    @Test
    fun `a holding account exposes a balance of its holding transaction even if they are other accounts`() {
        val account1 = Account(listOf(
            Transaction(10),
            Transaction(10)
        ))
        val account2 = Account(listOf(
            Transaction(10),
            Transaction(10)
        ))
        val masterAccount = Account(listOf(account1, account2))
        assertEquals(40, masterAccount.balance())
    }
}
