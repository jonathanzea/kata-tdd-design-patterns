package com.zea.composite.before

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class OverallAccountTest {

    @Test
    fun `exposes a balance of accounts`() {
        val account1 = Account(listOf(Transaction(10), Transaction(10)))
        val account2 = Account(listOf(Transaction(10), Transaction(10)))
        val overallAccount = OverallAccount(listOf(account1, account2))
        assertEquals(40, overallAccount.balance())
    }
}
