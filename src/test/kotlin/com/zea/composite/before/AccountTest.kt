package com.zea.composite.before

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class AccountTest {

    @Test
    fun `expose a balance of transactions`(){
        val account = Account(listOf(Transaction(10), Transaction(10)))
        assertEquals(20, account.balance())
    }
}
