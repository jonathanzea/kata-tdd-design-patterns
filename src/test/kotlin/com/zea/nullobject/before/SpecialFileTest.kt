package com.zea.nullobject.before

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

class SpecialFileTest {

    @Test
    fun `the status modified when the security is not null`() {
        val security = SecurityManager().getSecurity()
        val file = SpecialFile(security)
        file.open()
        assertTrue(file.passed)
        Assertions.assertEquals("File modified", file.status)
    }

    @Test
    fun `the status is not modified for null security`() {
        val file = SpecialFile(null)
        file.open()
        assertTrue(file.passed)
        Assertions.assertEquals("", file.status)
    }
}