package com.zea.nullobject.after
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

class SpecialFileTest {

    @Test
    fun `the status modified when the security is not null`() {
        val security = SecurityManager("Arbitrary Indicator").getSecurity()
        val file = SpecialFile(security)
        file.open()
        assertTrue(file.passed)
        assertEquals("File modified", file.status)
    }

    @Test
    fun `the status is not modified for null security`() {
        val security = SecurityManager(null).getSecurity()
        val file = SpecialFile(security)
        file.open()
        assertTrue(file.passed)
        assertEquals("", file.status)
    }
}