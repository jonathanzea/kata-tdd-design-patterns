package com.zea.command.before

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class CommandBeforeStateTest {

    @Test
    fun `a FileNewMenuItem type triggers a fileNewAction`() {
        val actionFactory = ActionFactory()
        val fileNewMenuItemExample = FileNewMenuItem()
        assertEquals("fileNewAction() triggered", actionFactory.actionPerformed(fileNewMenuItemExample))
    }

    @Test
    fun `a FileOpenMenuItem type triggers a fileOpenAction`() {
        val actionFactory = ActionFactory()
        val fileOpenMenuItem = FileOpenMenuItem()
        assertEquals("fileOpenAction() triggered", actionFactory.actionPerformed(fileOpenMenuItem))
    }

    @Test
    fun `a FileOpenRecentMenuItem type triggers a fileOpenRecentAction`() {
        val actionFactory = ActionFactory()
        val fileOpenMenuItem = FileOpenRecentMenuItem()
        assertEquals("fileOpenRecentAction() triggered", actionFactory.actionPerformed(fileOpenMenuItem))
    }

    @Test
    fun `a FileSaveMenuItem type triggers a fileSaveAction`() {
        val actionFactory = ActionFactory()
        val fileOpenMenuItem = FileSaveMenuItem()
        assertEquals("fileSaveAction() triggered", actionFactory.actionPerformed(fileOpenMenuItem))
    }
}

