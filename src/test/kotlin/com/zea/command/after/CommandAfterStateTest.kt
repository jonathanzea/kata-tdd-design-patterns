package com.zea.command.after

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class CommandAfterStateTest {

    private val actionFactory: ActionFactory = ActionFactory()

    @Test
    fun `the command behaviour for ActionEventFactory`() {
        val newFileActionMessage = "fileNewAction() triggered"
        assertEquals(newFileActionMessage, actionFactory.actionPerformed(FileNewMenuItem()))
        val fileOpenMenuItemMessage = "fileOpenMenuItem() triggered"
        assertEquals(fileOpenMenuItemMessage, actionFactory.actionPerformed(FileOpenMenuItem()))
        val fileOpenRecentMenuItemMessage = "fileOpenRecentAction() triggered"
        assertEquals(fileOpenRecentMenuItemMessage, actionFactory.actionPerformed(FileOpenRecentMenuItem()))
        val fileSaveMenuItemMessage = "fileSaveAction() triggered"
        assertEquals(fileSaveMenuItemMessage, actionFactory.actionPerformed(FileSaveMenuItem()))
    }
}